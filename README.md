# Kubernetes deployment resources

A collection of useful resources to set up a Kubernetes cluster from scratch.

## Cluster Setup

### Configure kubectl command line access

  `$ gcloud container clusters get-credentials test --zone europe-west2-a --project test-199214`

### Setup Cluster

  `$ setup.sh`

### Tools

You need to install the following tools in order to deploy apps to the test Kubernetes cluster.

* [Google Cloud SDK](https://cloud.google.com/sdk/)
* [Helm](https://helm.sh/)

### Configure kubectl command line access

  `$ gcloud container clusters get-credentials test --zone europe-west2-a --project test-199214`

### Dockerfile

Dockerfile example for a Sinatra app:

```
FROM ruby:2.2
LABEL maintainer="Your name <your.email@test.com>"

ENV APP_HOME /app

RUN apt-get update && \
    apt-get install -y net-tools

WORKDIR $APP_HOME
ADD Gemfile $APP_HOME/
ADD Gemfile.lock $APP_HOME/

RUN bundle install
ADD . $APP_HOME

EXPOSE 8080
CMD ["bundle", "exec", "ruby", "app.rb"]
```

### Usage

 * Clone this repository.

 * Run `sh kube-deploy.sh <service>` where `service` should match an test gitlab repo.

### New clusters configuration

#### 1. Initialize Helm
  `$ helm init`

#### 2. Installing Tiller with RBAC enabled

 `$ kubectl create serviceaccount tiller`

 `$ kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller`

 `$ kubectl patch deploy --namespace tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'`

 `$ helm init --service-account tiller --upgrade`

#### 3. Install The Nginx Ingress Controller

 `$ helm install --name nginx-ingress stable/nginx-ingress --set controller.hostNetwork=true,controller.kind=DaemonSet`

#### 4. Example Ingress yaml file

```
An example Ingress that makes use of the controller:

  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
    annotations:
      kubernetes.io/ingress.class: nginx
    name: example
    namespace: foo
  spec:
    rules:
      - host: www.example.com
        http:
          paths:
            - backend:
                serviceName: exampleService
                servicePort: 80
              path: /
    # This section is only required if TLS is to be enabled for the Ingress
    tls:
        - hosts:
            - www.example.com
          secretName: example-tls

 If TLS is enabled for the Ingress, a Secret containing the certificate and key must also be provided:

  apiVersion: v1
  kind: Secret
  metadata:
    name: example-tls
    namespace: foo
  data:
    tls.crt: <base64 encoded cert>
    tls.key: <base64 encoded key>
  type: kubernetes.io/tls
```

#### 5. Managing IP Addresses

  `$ gcloud compute addresses create test-ip --global`

  `$ gcloud compute addresses describe test-ip —global`

  `$ gcloud compute addresses delete test-ip --global`

#### 6. Create Ingress Object

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: joomla-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
spec:
  rules:
  - http:
      paths:
      - path: /
        backend:
          serviceName: ingress-example-joomla
          servicePort: 80
```

#### 7. Deploy a sample app in Kubernetes Engine

  $ kubectl run hello-app --image=gcr.io/google-samples/hello-app:1.0 --port=8080

  $ kubectl expose deployment hello-app

#### 8. TLS

   kube-lego is no longer used by the community to generate new certificates, we should use cert-manager instead.

   ` $ helm install --name cert-manager stable/cert-manager `

  [cert-manager](https://cert-manager.readthedocs.io/en/latest/getting-started/3-configuring-first-issuer.html)
