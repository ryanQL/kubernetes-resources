#!/bin/zsh

kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'

helm init --service-account tiller --upgrade
helm install --name my-release stable/cert-manager
helm install --name nginx-ingress stable/nginx-ingress --set controller.hostNetwork=true,controller.kind=DaemonSet

kubectl run hello-app --image=gcr.io/google-samples/hello-app:1.0 --port=8080
kubectl expose deployment hello-app
kubectl apply -f ingress/nginx-ingress.production.yaml
