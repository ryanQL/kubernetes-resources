#!/bin/zsh

export SERVICE=$1
export VERSION=$2
export PORT=$3
export TARGET_PORT=$4
export PROJECT_ID="$(gcloud config get-value project -q)"
export HOST="api.test.com"
export CONTAINER_IMAGE="gcr.io/${PROJECT_ID}/${SERVICE}:${VERSION}"

cd deploy/${SERVICE}
gcloud config set project test-199214
git clone https://gitlab.com/test/${SERVICE}
source .env

docker build -t ${CONTAINER_IMAGE} .
gcloud docker -- push ${CONTAINER_IMAGE}
kubectl delete deployments ${SERVICE}
kubectl delete service ${SERVICE}
kubectl run ${SERVICE} --image=${CONTAINER_IMAGE}
kubectl expose deployment ${SERVICE} --type=ClusterIP --port ${PORT} --target-port ${TARGET_PORT}
kubectl scale deployment ${SERVICE} --replicas=3
