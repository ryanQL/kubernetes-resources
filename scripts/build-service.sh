#!/bin/zsh

PROJECT=gcr.io/test-199214
PORT=8080
if [ $# -eq 0 ]
  then
    echo "
Creates a Docker image ready for Kubernetes.

Usage:

    ./scripts/build-service.sh <name> <version>

    e.g. build-service.sh developer-docs v1
    "
  else
    cd deploy/$1
    docker build -t $PROJECT/$1:$2 .
    docker run --rm -d -p $3:$4 $PROJECT/$1:$2 
fi
